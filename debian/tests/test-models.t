#!/usr/bin/perl
# This test simply checks that all model files from upstream's share/ 
# directory are correctly loaded by latest version of libai-fann-perl
use strict;
use warnings;

use Test::More tests => 14;

BEGIN { use_ok('AI::FANN') };

foreach (("a", "b", "bb", "bu", "fa", "fb", "fbb", "fbu", "fub", "fuu", "u", 
        "ub", "uu")) {
    my $filename = "/usr/share/reprof/".$_.".model";
    print "Try to load ".$filename." to libai-fann-perl\n";
    ok(defined AI::FANN->new_from_file($filename), 
      "Load ".$filename." to libai-fann-perl");
}
